<?php

namespace Tests\Unit;

use Tests\TestCase;

class SumOfDigitsTest extends TestCase
{
    /**
     * @return void
     */
    public function testSumOfDigits()
    {
        $this->assertEquals(5, $this->sumOfDigits(23));
        $this->assertEquals(19, $this->sumOfDigits(496));
        $this->assertEquals(57, $this->sumOfDigits(123456878445));
    }

    /**
     * 入力された10進数を加算して返す
     *  23 → 2 + 3 = 5
     *
     * @param int $digit
     * @return int
     */
    function sumOfDigits(int $digit): int
    {
        $sum = 0;
        $numbStr = (string) $digit;
        for ($i = 0; $i < strlen($numbStr); $i++) {
            $sum += (int)$numbStr[$i];
        }
        return $sum;
    }
}
