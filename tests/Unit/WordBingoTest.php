<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class WordBingoTest extends TestCase
{
    const PATTERN1 = <<<__LONG_TEXT__
3
apple orange cube
batch web cloud
sql http https
7
web
https
windows
batch
keyboard
apple
cpu
__LONG_TEXT__;

    const PATTERN2 = <<<__LONG_TEXT__
3
cpp kotlin typescript
csharp ruby php
go rust dart
5
java
delphi
fortran
haskell
python
__LONG_TEXT__;


    const PATTERN3 = <<<__LONG_TEXT__
4
beer wine gin vodka
beef chicken pork seafood
ant bee ladybug beetle
bear snake dog camel
7
be
bear
bee
beef
been
beer
beetle
__LONG_TEXT__;

    /**
     * @return void
     */
    public function testWordBingo()
    {
        // 左上から右下にかけてななめに3個の印が並び、ビンゴが成立しています。
        $this->assertEquals('yes', $this->wordBingo(self::PATTERN1));
        // 印は1つも付いていません。
        $this->assertEquals('no', $this->wordBingo(self::PATTERN2));
        // 4個の印が並ぶ列はありませんので、ビンゴは成立していません。
        $this->assertEquals('no', $this->wordBingo(self::PATTERN3));
    }

    /**
     * @param string $input
     * @return string
     */
    function wordBingo(string $input): string
    {
        $arr = $this->generateBingoBoardAndSearchIndexs($input);
        $bingoBoard = $arr[0];
        $searchIndexs = $arr[1];
        $boardSize = $arr[2];
        $connerPosition = [[0, 0], [0, $boardSize], [$boardSize, 0], [$boardSize, $boardSize]];
        $border_match_positions = array();
        for ($i = 0; $i < $boardSize; $i++) {
            if (in_array($bingoBoard[0][$i], $searchIndexs)) {
                array_push($border_match_positions, [$i, 0]);
            }
        }
        for ($i = 1; $i < $boardSize; $i++) {
            if (in_array($bingoBoard[$i][0], $searchIndexs)) {
                array_push($border_match_positions, [$i, 0]);
            }
        }
        if (count($border_match_positions) > 0) {
            foreach ($border_match_positions as &$position) {
                if ($position[0] === 0 &&  $position[1] === 0) {
                    $count = 0;
                    for ($i = 0; $i < $boardSize; $i++) {
                        if (in_array($bingoBoard[$i][$i], $searchIndexs)) {
                            $count++;
                        }
                    }
                    if ($count === $boardSize) {
                        return 'yes';
                    }
                    return 'no';
                }
                if ($position[0] === 0 &&  $position[1] === $boardSize - 1) {
                    $count = 0;
                    for ($i = $boardSize - 1; $i > 0; $i--) {
                        if (in_array($bingoBoard[$boardSize - 1 - $i][$i], $searchIndexs)) {
                            $count++;
                        }
                    }
                    if ($count === $boardSize) {
                        return 'yes';
                    }
                    return 'no';
                }
                if ($position[0] === 0) {
                    $count = 0;
                    for ($i = 0; $i < $boardSize; $i++) {
                        if (in_array($bingoBoard[0][$i], $searchIndexs)) {
                            $count++;
                        }
                    }
                    if ($count === $boardSize) {
                        return 'yes';
                    }
                    $count = 0;
                    for ($i = 0; $i < $boardSize; $i++) {
                        if (in_array($bingoBoard[$i][$position[1]], $searchIndexs)) {
                            $count++;
                        }
                    }
                    if ($count === $boardSize) {
                        return 'yes';
                    }
                    return 'no';
                } else if ($position[1] === 0 && $position[0] > 0) {
                    $count = 0;
                    for ($i = 0; $i < $boardSize; $i++) {
                        if (in_array($bingoBoard[$position[0]][$i], $searchIndexs)) {
                            $count++;
                        }
                    }
                    if ($count === $boardSize) {
                        return 'yes';
                    }
                    return 'no';
                } else {
                    return 'no';
                }
            }
        } else {
            return 'no';
        }
    }

    function generateBingoBoardAndSearchIndexs(string $input): array
    {
        $arr = explode("\n", $input);
        $bingo_board = array(array());
        $arr_length = (int) $arr[0];
        $index_arr = array();
        for ($i = 0; $i < $arr_length; $i++) {
            $sub_arr = explode(" ", $arr[$i + 1]);
            $bingo_board[$i] = $sub_arr;
        }
        for ($i = $arr_length + 1; $i < count($arr) - 1; $i++) {
            array_push($index_arr, $arr[$i + 1]);
        }
        return [$bingo_board, $index_arr, $arr_length];
    }
}
