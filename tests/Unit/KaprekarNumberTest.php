<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class KaprekarNumberTest extends TestCase
{
    /**
     * @return void
     */
    public function testKaprekarNumber()
    {
        $this->assertEquals(0, $this->kaprekarNumber(0));
        $this->assertEquals(495, $this->kaprekarNumber(100));
        $this->assertEquals(6174, $this->kaprekarNumber(500));
        $this->assertEquals(631764, $this->kaprekarNumber(549946));
        //$this->assertEquals(63317664, $this->kaprekarNumber(631765));
        // $this->assertEquals(97508421, $this->kaprekarNumber(63317665));
        // $this->assertEquals(6333176664, $this->kaprekarNumber(1000000000));
    }

    /**
     * 指定された数以上の最も小さいカプレカ数を返す
     *
     * @param int $n
     * @return int
     */
    function kaprekarNumber(int $n): int
    {
        if ($n === 0) {
            return 0;
        }

        while (true) {
            if ($this->isKaprekarNumber($n)) {
                return $n;
            }
            $n++;
        }
    }

    function isKaprekarNumber(int $number): bool
    {
        if ($this->createMaxNumberByDigits($number) - $this->createMinNumberByDigits($number) === $number) {
            return true;
        } else {
            return false;
        }
    }

    function createMaxNumberByDigits(int $number): int
    {
        $digit_array = str_split($number);
        rsort($digit_array);
        return (int)implode("", $digit_array);
    }

    function createMinNumberByDigits(int $number): int
    {
        $digit_array = str_split($number);
        sort($digit_array);
        return (int)implode("", $digit_array);
    }
}
