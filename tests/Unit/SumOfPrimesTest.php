<?php

namespace Tests\Unit;

use Tests\TestCase;

class SumOfPrimesTest extends TestCase
{
    /**
     * @return void
     */
    public function testExample()
    {
        $this->assertEquals(2, $this->sumOfPrimes(1));
        $this->assertEquals(5, $this->sumOfPrimes(2));
        $this->assertEquals(28, $this->sumOfPrimes(5));
        $this->assertEquals(129, $this->sumOfPrimes(10));
        $this->assertEquals(3682913, $this->sumOfPrimes(1000));
        $this->assertEquals(16274627, $this->sumOfPrimes(2000));
    }

    /**
     * 指定された数の素数を合計して返す
     * 素数はかならず 2 から値の小さい順に加算する
     *
     * @param int $n
     * @return int
     */
    function sumOfPrimes(int $n): int
    {
        $prime_count = 1;
        $count = 2;
        $sum = 0;
        while ($prime_count <= $n) {
            if ($this->isPrime($count)) {
                $sum += $count;
                $prime_count++;
                $count++;
            } else {
                $count++;
            }
        }
        return $sum;
    }

    function isPrime(int $num): bool
    {
        if ($num <= 1) {
            return false;
        }
        for ($i = 2; $i <= sqrt($num); $i++) {
            if ($num % $i === 0)
                return false;
        }
        return true;
    }
}
