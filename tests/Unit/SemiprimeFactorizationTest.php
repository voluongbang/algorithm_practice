<?php

namespace Tests\Unit;

use Tests\TestCase;

class SemiprimeFactorizationTest extends TestCase
{
    /**
     * @return void
     */
    public function testSemiprimeFactorization()
    {
        $this->assertEquals('2 3', $this->semiprimeFactorization(6));
        $this->assertEquals('3 5', $this->semiprimeFactorization(15));
        $this->assertEquals('5 7', $this->semiprimeFactorization(35));
        $this->assertEquals('7 11', $this->semiprimeFactorization(77));
        $this->assertEquals('3571 7213', $this->semiprimeFactorization(25757623));
        $this->assertEquals('27449 53507', $this->semiprimeFactorization(1468713643));
    }

    /**
     * 指定された数を因数分解し、小さい順に返す
     * *Pollard's rho algorithm
     *
     * @param int $n
     * @return string
     */
    function semiprimeFactorization(int $n): string
    {
        $x = 2;
        $y = 2;
        $p = 1;

        while ($p === 1) {
            $x = ($this->g($x, $n) + $x) % $n;
            $y = ($this->g($y, $n) + $y) % $n;
            $y = ($this->g($y, $n) + $y) % $n;

            $p = $this->gcd(abs($x - $y), $n);
        }

        if ($p === $n) {
            return "Failed to factorize";
        } else {
            $q = $n / $p;
            $result = ($p < $q) ? \strval($p) . ' ' . \strval($q) : \strval($q) . ' ' . \strval($p);
            return $result;
        }
    }


    /**
     * *Find GCD of 2 integers
     *
     * @param int $a
     * @param int $b
     * @return string
     */
    function gcd($a, $b)
    {
        while ($b != 0) {
            $t = $b;
            $b = $a % $b;
            $a = $t;
        }
        return $a;
    }

    /**
     * *Find g(x) = (x^2 + 1) mod n
     *
     * @param int $a
     * @param int $b
     * @return string
     */
    function g($x, $n)
    {
        return ($x * $x + 1) % $n;
    }
}
